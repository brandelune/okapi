@echo off

if not exist ".\superpom" cd ..
if not exist ".\superpom" cd ..

call mvn clean -f integration-tests
call ant clean -f deployment/maven
call mvn clean -f okapi-ui/swt/core-ui
call mvn clean

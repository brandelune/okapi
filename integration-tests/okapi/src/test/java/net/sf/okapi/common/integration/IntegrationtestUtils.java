package net.sf.okapi.common.integration;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static java.util.Arrays.asList;

public final class IntegrationtestUtils {
	public final static FileLocation ROOT = FileLocation.fromClass(IntegrationtestUtils.class);

	public static Collection<File> getTestFiles(File dir, final List<String> extensions)
			throws URISyntaxException {
		FilenameFilter filter = (dir1, name) -> {
			for (String e : extensions) {
				if (name.endsWith(e)) {
					return true;
				}
			}
			return false;
		};
		return FileUtil.getFilteredFiles(dir, filter, true);
	}

	public static Collection<File> getTestFilesNoRecurse(File dir, final List<String> extensions) {
		FilenameFilter filter = (dir1, name) -> {
			for (String e : extensions) {
				if (name.endsWith(e)) {
					return true;
				}
			}
			return false;
		};
		return FileUtil.getFilteredFiles(dir, filter, false);
	}

	public static Collection<File> getSubDirsNoRecurse(File dir) throws URISyntaxException {
		FilenameFilter filter = (d, n) -> new File(d, n).isDirectory();
		return FileUtil.getFilteredFiles(dir, filter, false);
	}

	public static Collection<File> getSubDirs(File dir) throws URISyntaxException {
		FilenameFilter filter = (d, n) -> new File(d, n).isDirectory();
		return FileUtil.getFilteredFiles(dir, filter, true);
	}

	public static File getSecondaryConfigFile(File dir,	String primaryConfigName) throws URISyntaxException {
		// secondary must have .secondary extension
		List<String> extensions = asList(".secondary.fprm", ".secondary.its");
		Collection<File> secondaries = getTestFiles(dir, extensions);
		for (File file : secondaries) {
			String p = Util.getFilename(primaryConfigName, false);
			// return the first one - only one .secondary per subdir
			return file;
		}

		return null;
	}

	public static Collection<File> getConfigFile(File dir)
			throws URISyntaxException {
		List<String> extensions = asList(".fprm", ".its");
		Collection<File> primaries = new LinkedList<File>();
		for (File f : getTestFiles(dir, extensions)) {
			String p = Util.getFilename(f.getName(), true);
			if (p.endsWith(".secondary.fprm") || p.endsWith(".secondary.its")) {
				continue;
			}
			primaries.add(f);
		}

		return primaries;
	}
	
	public static File asFile(String rootPath, final String fileName) throws URISyntaxException, IOException {
		try (JarFile jarFile = new JarFile(rootPath)) {
			final Enumeration<JarEntry> entries = jarFile.entries();
			while (entries.hasMoreElements()) {
				final JarEntry entry = entries.nextElement();
				final String name = entry.getName();
				if (fileName.equals(name)) {
					jarFile.close();
					return new File(Util.buildPath(rootPath, name));
				}
			}
		} catch (IOException | SecurityException e) {
			for (String fname : new File(rootPath).list((dir, name) -> {
				if (fileName.equals(name)) {
					return true;
				}
				return false;
			})) {
				String fpath = Util.buildPath(rootPath, fname);
				return new File(fpath);
			}
		}

		return null;
	}

	static public ArrayList<Event> getEvents(IFilter filter, RawDocument rd, IParameters parameters) {
		ArrayList<Event> list = new ArrayList<Event>();
		try {
			if (parameters != null) {
				filter.setParameters(parameters);
			}
			filter.open(rd);
			while (filter.hasNext()) {
				list.add(filter.next());
			}
		} finally {
			filter.close();
			rd.close();
		}
		return list;
	}

	static public ArrayList<Event> getNonTextUnitEvents(IFilter filter, RawDocument rd, IParameters parameters) {
		ArrayList<Event> list = new ArrayList<Event>();
		try {
			if (parameters != null) {
				filter.setParameters(parameters);
			}
			filter.open(rd);
			while (filter.hasNext()) {
				Event e = filter.next();
				if (!e.isTextUnit()) {
					list.add(e);
				}
			}
		} finally {
			filter.close();
			rd.close();
		}
		return list;
	}

	static public ArrayList<ITextUnit> getTextUnitEvents(IFilter filter, RawDocument rd, IParameters parameters) {
		ArrayList<ITextUnit> list = new ArrayList<>();
		try {
			if (parameters != null) {
				filter.setParameters(parameters);
			}
			filter.open(rd);
			while (filter.hasNext()) {
				Event e = filter.next();
				if (e.isTextUnit()) {
					list.add(e.getTextUnit());
				}
			}
		} finally {
			filter.close();
			rd.close();
		}
		return list;
	}

	static public ArrayList<Event> getEvents(String snippet, IFilter filter, IParameters parameters) {
		ArrayList<Event> list = new ArrayList<Event>();

		if (parameters != null) {
			filter.setParameters(parameters);
		}
		try (RawDocument rawDoc = new RawDocument(snippet, LocaleId.ENGLISH)) {
			filter.open(rawDoc);
			while (filter.hasNext()) {
				Event event = filter.next();
				list.add(event);
			}
		} finally {
			filter.close();
		}
		return list;
	}

	public static boolean deleteDirRecursive(String path) {
		File d;
		d = new File(path);
		if (d.isDirectory()) {
			String[] children = d.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDirRecursive(d.getAbsolutePath()
						+ File.separator + children[i]);
				if (!success) {
					return false;
				}
			}
		}
		if (d.exists())
			return d.delete();
		else
			return true;
	}
}

package net.sf.okapi.common.integration;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.filters.FilterTestUtil;
import net.sf.okapi.common.resource.ITextUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;

import javax.xml.parsers.ParserConfigurationException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;

final public class FileComparator {
	private final static Logger LOGGER = LoggerFactory.getLogger(FileComparator.class);

	private FileComparator() {
		// don't instantiate this utility class
	}

	public static class Utf8FilePerLineComparator implements IComparator<Path> {
		@Override
		public boolean compare(Path actual, Path expected) {
			return new FileCompare().compareFilesPerLines(actual.toString(), expected.toString(), StandardCharsets.UTF_8.name());
		}
	}

	public static class EventComparator implements IComparator<List<Event>> {
		@Override
		public boolean compare(List<Event> actual, List<Event> expected) {
			return RoundTripUtils.compareEvents(actual, expected, false, true, true, false);
		}
	}

	public static class EventComparatorTextUnitOnly implements IComparator<List<ITextUnit>> {
		@Override
		public boolean compare(List<ITextUnit> actual, List<ITextUnit> expected) {
			return RoundTripUtils.compareTextUnits(actual, expected, true);
		}
	}

	public static class EventComparatorWithWhitespace implements IComparator<List<Event>>  {
		@Override
		public boolean compare(List<Event> actual, List<Event> expected) {
			return RoundTripUtils.compareEvents(actual, expected, true, false, false, false);
		}
	}

	public static class EventComparatorIgnoreSegmentation implements IComparator<List<Event>>  {
		@Override
		public boolean compare(List<Event> actual, List<Event> expected) {
			return RoundTripUtils.compareEvents(actual, expected, true, false, false, true);
		}
	}

	public static class ArchiveComparator implements IComparator<Path> {
		@Override
		public boolean compare(Path actual, Path expected) {
			final ArchiveFileCompare ac = new ArchiveFileCompare(new XmlComparator<InputStream>());
			return ac.compareFiles(actual, expected);
		}
	}

	public static class XmlComparator<T> implements IComparator<T> {
		@Override
		public boolean compare(T actual, T expected) {
			final Diff documentDiff;
			try {
				documentDiff = FilterTestUtil.compareXml(actual, expected, true);
				final boolean pass = !documentDiff.hasDifferences();
				if (!pass) {
					LOGGER.error("XML Differences:");
					for (final Difference d : documentDiff.getDifferences()) {
						LOGGER.error("+ {}", d.toString());
					}
				}
				return pass;
			} catch (final ParserConfigurationException e) {
				throw new OkapiBadFilterInputException("XML Parse Error: ", e);
			}
		}
	}
}

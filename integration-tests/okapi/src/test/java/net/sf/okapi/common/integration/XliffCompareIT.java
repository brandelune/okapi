package net.sf.okapi.common.integration;

import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.exceptions.OkapiException;
import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class XliffCompareIT extends BaseRoundTripIT {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Rule
	public ErrorCollector errCol = new ErrorCollector();

	public XliffCompareIT(final String configId, final String dirName, final List<String> extensions) {
		super(configId, dirName, extensions);
	}

	public XliffCompareIT(final String configId, final String dirName,
						  final List<String> extensions, final LocaleId defaultTargetLocale) {
		super(configId, dirName, extensions, defaultTargetLocale);
	}

	@Override
	protected void runTest(final boolean detectLocales, final File file, File subDir, final String customConfigPath, final IComparator comparator) {
		final String f = file.getName();
		final String root = file.getParent() + File.separator;
		final String xliff = root + f + xliffExtractedExtension;
		final String original = root + f;
		final String currentXliffRoot = new File(getClass().getResource("/XLIFF_PREV/dummy.txt").getPath()).getParent();
		final String sd = ((subDir == null) ? "" : subDir.getName() + "/");
		final String xliffPrevious = currentXliffRoot + dirName + sd + f + xliffExtractedExtension;
		LocaleId source = LocaleId.ENGLISH;
		LocaleId target = defaultTargetLocale;
		if (detectLocales) {
			final List<String> locales = FileUtil.guessLanguages(file.getAbsolutePath());
			if (locales.size() >= 1) {
				source = LocaleId.fromString(locales.get(0));
			}
			if (locales.size() >= 2) {
				target = LocaleId.fromString(locales.get(1));
			}
		}

		try {
			logger.info(f);
			RoundTripUtils.extract(source, target, original, xliff, configId, customConfigPath, false);
			assertTrue("Compare Events: " + f, comparator.compare(Paths.get(xliff), Paths.get(xliffPrevious)));
		} catch (final Throwable e) {
			if (!knownFailingFiles.contains(f)) {
				errCol.addError(new OkapiException(f, e));
			} else {
				logger.info("Ignored known failing file: {}", f);
			}
		}
	}
}

package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.doxygen.DoxygenFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class DoxygenXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_doxygen";
	private static final String DIR_NAME = "/doxygen/";
	private static final List<String> EXTENSIONS = Arrays.asList(".h", ".py");

	public DoxygenXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS);
	}

	@Before
	public void setUp() throws Exception {
		filter = new DoxygenFilter();
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}

	@Test
	public void doxygenXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}

/*===========================================================================
  Copyright (C) 2009-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.StringUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.EndSubfilter;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.IMultilingual;
import net.sf.okapi.common.resource.INameable;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.resource.StartSubfilter;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

/**
 * Driver to test filter output.
 */
public class FilterTestDriver {
	/*
	 * In the CI builds we want to see more than
	 * "the lists of events are different". So we will log the "interesting info" at
	 * warning level . We make sure that if a difference was reported by another
	 * method we don't log it again. For example we don't report after
	 * compareTextUnit because compareTextUnit itself did it already, and more
	 * accurately.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(FilterTestDriver.class);
	private static final Pattern PATTERN_WHITESPACE = Pattern
			.compile("(\\p{Zs}|\\p{Z})+|(xml:space=\"preserve\")|(<w:r>)|(<w:t>)|(</w:t>)|(</w:r>)");
	private boolean showSkeleton = true;
	private int displayLevel = 0;
	private boolean ok;

	/**
	 * Process a RawDocument with the specified filter and return a list of all
	 * events it produces.
	 * 
	 * @param filter     filter to use
	 * @param rd         RawDocument to process
	 * @param parameters parameters to use with this filter (may be null)
	 * @return list of events
	 */
	static public ArrayList<Event> getEvents(IFilter filter, RawDocument rd, IParameters parameters) {
		ArrayList<Event> list = new ArrayList<>();
		if (parameters != null) {
			filter.setParameters(parameters);
		}
		filter.open(rd);
		while (filter.hasNext()) {
			list.add(filter.next());
		}
		filter.close();
		if (LOGGER.isDebugEnabled()) {
			FilterUtil.logDebugEvents(list, LOGGER);
		}
		return list;
	}

	/**
	 * Process String content with the specified filter and return a list of all
	 * events it produces.
	 * 
	 * @param filter     filter to use
	 * @param snippet    text to process
	 * @param parameters parameters to use with this filter (may be null)
	 * @param srcLocale  source locale for the content (may be null)
	 * @param tgtLocale  target locale for the content (may be null)
	 * @return list of events
	 */
	static public ArrayList<Event> getEvents(IFilter filter, String snippet, IParameters parameters, LocaleId srcLocale,
			LocaleId tgtLocale) {
		return getEvents(filter, new RawDocument(snippet, srcLocale, tgtLocale), parameters);
	}

	/**
	 * Process String content with the specified filter and return a list of all
	 * events it produces.
	 * 
	 * @param filter    filter to use
	 * @param snippet   text to process
	 * @param srcLocale source locale for the content (may be null)
	 * @param tgtLocale target locale for the content (may be null)
	 * @return list of events
	 */
	static public ArrayList<Event> getEvents(IFilter filter, String snippet, LocaleId srcLocale, LocaleId tgtLocale) {
		return getEvents(filter, snippet, null, srcLocale, tgtLocale);
	}

	/**
	 * Process String content with the specified filter and return a list of all
	 * events it produces.
	 * 
	 * @param filter    filter to use
	 * @param snippet   text to process
	 * @param srcLocale source locale for the content (may be null)
	 * @return list of events
	 */
	static public ArrayList<Event> getEvents(IFilter filter, String snippet, LocaleId srcLocale) {
		return getEvents(filter, snippet, srcLocale, null);
	}

	/**
	 * Return the provided list of events, with any MultiEvents expanded into
	 * multiple individual events.
	 * 
	 * @param events events to be expanded
	 * @return expanded events
	 */
	static public ArrayList<Event> expandMultiEvents(List<Event> events) {
		ArrayList<Event> expanded = new ArrayList<>();
		for (Event event : events) {
			if (event.isMultiEvent()) {
				for (Event e : event.getMultiEvent()) {
					expanded.add(e);
				}
			}
			expanded.add(event);
		}
		return expanded;
	}

	static public boolean laxCompareEvent(Event manual, Event generated) {
		if (generated.getEventType() != manual.getEventType()) {
			return false;
		}
		IResource mr = manual.getResource();
		IResource gr = generated.getResource();

		if (mr != null && gr != null && mr.getSkeleton() != null && gr.getSkeleton() != null) {
			if (!(mr.getSkeleton().toString().equals(gr.getSkeleton().toString()))) {
				return false;
			}
		}

		switch (generated.getEventType()) {
		case DOCUMENT_PART:
			DocumentPart mdp = (DocumentPart) mr;
			DocumentPart gdp = (DocumentPart) gr;
			if (mdp.isReferent() != gdp.isReferent()) {
				return false;
			}
			if (mdp.isTranslatable() != gdp.isTranslatable()) {
				return false;
			}
			if (!(mdp.getPropertyNames().equals(gdp.getPropertyNames()))) {
				return false;
			}
			for (String propName : gdp.getPropertyNames()) {
				Property gdpProp = gdp.getProperty(propName);
				Property mdpProp = mdp.getProperty(propName);
				if (gdpProp.isReadOnly() != mdpProp.isReadOnly()) {
					return false;
				}
				if (!gdpProp.getValue().equals(mdpProp.getValue())) {
					return false;
				}
			}

			if (!(mdp.getSourcePropertyNames().equals(gdp.getSourcePropertyNames()))) {
				return false;
			}
			for (String propName : gdp.getSourcePropertyNames()) {
				Property gdpProp = gdp.getSourceProperty(propName);
				Property mdpProp = mdp.getSourceProperty(propName);
				if (gdpProp.isReadOnly() != mdpProp.isReadOnly()) {
					return false;
				}
				if (!gdpProp.getValue().equals(mdpProp.getValue())) {
					return false;
				}
			}
			break;

		case TEXT_UNIT:
			ITextUnit mtu = (ITextUnit) mr;
			ITextUnit gtu = (ITextUnit) gr;

			// Resource-level properties
			if (!(mtu.getPropertyNames().equals(gtu.getPropertyNames()))) {
				return false;
			}
			for (String propName : gtu.getPropertyNames()) {
				Property gtuProp = gtu.getProperty(propName);
				Property mtuProp = mtu.getProperty(propName);
				if (gtuProp.isReadOnly() != mtuProp.isReadOnly()) {
					return false;
				}
				if (!gtuProp.getValue().equals(mtuProp.getValue())) {
					return false;
				}
			}

			// Source properties
			if (!(mtu.getSourcePropertyNames().equals(gtu.getSourcePropertyNames()))) {
				return false;
			}
			for (String propName : gtu.getSourcePropertyNames()) {
				Property gtuProp = gtu.getSourceProperty(propName);
				Property mtuProp = mtu.getSourceProperty(propName);
				if (gtuProp.isReadOnly() != mtuProp.isReadOnly()) {
					return false;
				}
				if (!gtuProp.getValue().equals(mtuProp.getValue())) {
					return false;
				}
			}

			String tmp = mtu.getName();
			if (!Objects.equals(tmp, gtu.getName())) {
				return false;
			}

			tmp = mtu.getType();
			if (!Objects.equals(tmp, gtu.getType())) {
				return false;
			}

			if (mtu.isTranslatable() != gtu.isTranslatable()) {
				return false;
			}
			if (mtu.isReferent() != gtu.isReferent()) {
				return false;
			}
			if (mtu.preserveWhitespaces() != gtu.preserveWhitespaces()) {
				return false;
			}
			if (!(mtu.toString().equals(gtu.toString()))) {
				return false;
			}

			if (mtu.getSource().getUnSegmentedContentCopy().getCodes().size() != gtu.getSource()
					.getUnSegmentedContentCopy().getCodes().size()) {
				return false;
			}
			int i = -1;
			for (Code c : mtu.getSource().getUnSegmentedContentCopy().getCodes()) {
				i++;
				if ((c.getType() != null)
						&& !c.getType().equals(gtu.getSource().getUnSegmentedContentCopy().getCode(i).getType())) {
					return false;
				}
			}
			break;
		default:
			break;
		}

		return true;
	}

	static public boolean laxCompareEvents(List<Event> manual, List<Event> generated) {
		if (manual.size() != generated.size()) {
			return false;
		}

		Iterator<Event> manualIt = manual.iterator();
		for (Event ge : generated) {
			Event me = manualIt.next();
			if (!laxCompareEvent(me, ge)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Compares the codes of two text fragments so in-lines codes that have the same
	 * IDs have also the same content (getData()), except for opening/closing cases.
	 * 
	 * @param tf1 the base fragment.
	 * @param tf2 the fragment to compare with the base fragment.
	 */
	static public void checkCodeData(TextFragment tf1, TextFragment tf2) {
		List<Code> srcCodes = tf1.getCodes();
		List<Code> trgCodes = tf2.getCodes();
		for (Code srcCode : srcCodes) {
			for (Code trgCode : trgCodes) {
				// Same ID must have the same content, except for open/close
				// FIXME: Is the above assumption true? What about target codes that have been
				// changed?
				if (srcCode.getId() == trgCode.getId()) {
					switch (srcCode.getTagType()) {
					case OPENING:
						if (trgCode.getTagType() == TagType.CLOSING)
							break;
						assertEquals(srcCode.getData(), trgCode.getData());
						break;
					case CLOSING:
						if (trgCode.getTagType() == TagType.OPENING)
							break;
						assertEquals(srcCode.getData(), trgCode.getData());
						break;
					default:
						assertEquals(srcCode.getData(), trgCode.getData());
						break;
					}
				}
			}
		}
	}

	static public boolean compareEvent(Event actual, Event expected, boolean includeSkeleton,
			boolean ignoreSkelWhitespace, boolean ignoreFragmentWhitespace, boolean ignoreSegmentation) {
		if (expected.getEventType() != actual.getEventType()) {
			LOGGER.error("Event type difference: {} vs {}", expected.getEventType(), actual.getEventType());
			return false;
		}

		switch (expected.getEventType()) {
		case DOCUMENT_PART:
			DocumentPart mdp = (DocumentPart) actual.getResource();
			DocumentPart gdp = (DocumentPart) expected.getResource();
			if (!compareIResource(mdp, gdp, includeSkeleton, ignoreSkelWhitespace)) {
				return false;
			}
			if (!compareINameable(mdp, gdp, ignoreFragmentWhitespace)) {
				return false;
			}
			if (mdp.isReferent() != gdp.isReferent()) {
				LOGGER.error("DOCUMENT_PART isReferent difference: {} vs {}", mdp.isReferent(), gdp.isReferent());
				return false;
			}
			break;

		case START_GROUP:
			StartGroup sg1 = (StartGroup) actual.getResource();
			StartGroup sg2 = (StartGroup) expected.getResource();
			if (!compareIResource(sg1, sg2, includeSkeleton, ignoreSkelWhitespace)) {
				return false;
			}
			if (!compareINameable(sg1, sg2, ignoreFragmentWhitespace)) {
				return false;
			}
			if (sg1.isReferent() != sg2.isReferent()) {
				LOGGER.error("START_GROUP isReferent difference: {} vs {}", sg1.isReferent(), sg2.isReferent());
				return false;
			}
			break;

		case END_GROUP:
			if (!compareIResource(actual.getResource(), expected.getResource(), includeSkeleton,
					ignoreSkelWhitespace)) {
				return false;
			}
			break;

		case START_SUBFILTER:
			StartSubfilter ssf1 = (StartSubfilter) actual.getResource();
			StartSubfilter ssf2 = (StartSubfilter) expected.getResource();
			if (!compareIResource(ssf1, ssf2, includeSkeleton, ignoreSkelWhitespace)) {
				return false;
			}
			if (!compareINameable(ssf1, ssf2, ignoreFragmentWhitespace)) {
				return false;
			}
			if (ssf1.isReferent() != ssf2.isReferent()) {
				LOGGER.error("START_SUBFILTER isReferent difference: {} vs {}", ssf1.isReferent(), ssf2.isReferent());
				return false;
			}
			break;

		case END_SUBFILTER:
			EndSubfilter esf1 = (EndSubfilter) actual.getResource();
			EndSubfilter esf2 = (EndSubfilter) expected.getResource();
			if (!compareIResource(esf1, esf2, includeSkeleton, ignoreSkelWhitespace)) {
				return false;
			}
			break;
		case TEXT_UNIT:
			ITextUnit tu = actual.getTextUnit();
			if (!compareTextUnit(tu, expected.getTextUnit(), ignoreFragmentWhitespace, ignoreSegmentation)) {
				LOGGER.error("Text unit difference, tu id={}", tu.getId());
				return false;
			}
			break;
		default:
			break;
		}

		return true;
	}

	static public boolean compareEvents(List<Event> actual, List<Event> expected) {
		return compareEvents(actual, expected, true);
	}

	static public boolean compareEvents(List<Event> actual, List<Event> expected, boolean includeSkeleton) {
		return compareEvents(actual, expected, includeSkeleton, false, false, false);
	}

	static public boolean compareEvents(List<Event> actual, List<Event> expected, boolean includeSkeleton,
			boolean ignoreSkelWhitespace, boolean ignoreFragmentWhitespace, boolean ignoreSegmentation) {
		if (actual.size() != expected.size()) {
			LOGGER.error("Less events in first list: {} vs {}", actual.size(), expected.size());
			return false;
		}

		Event event1, event2;
		int i = 0;
		while (i < actual.size()) {
			event1 = actual.get(i);
			if (i >= expected.size()) {
				LOGGER.error("Less events in second list: {} vs {}", i, expected.size());
				return false;
			}
			event2 = expected.get(i);
			if (!compareEvent(event1, event2, includeSkeleton, ignoreSkelWhitespace, ignoreFragmentWhitespace, ignoreSegmentation)) {
				return false;
			}
			i++;
		}

		return true;
	}

	static public boolean compareTextUnits(List<ITextUnit> actual, List<ITextUnit> expected, boolean ignoreFragmentWhitespace) {
		if (actual.size() != expected.size()) {
			LOGGER.error("Less events in first list: {} vs {}", actual.size(), expected.size());
			return false;
		}

		ITextUnit event1, event2;
		int i = 0;
		while (i < actual.size()) {
			event1 = actual.get(i);
			event2 = expected.get(i);
			if (!compareTextUnit(event1, event2, ignoreFragmentWhitespace, false)) {
				return false;
			}
			i++;
		}

		return true;
	}

	static public boolean compareEvents(List<Event> actual, List<Event> expected, List<Event> subDocEvents,
			boolean includeSkeleton, boolean ignoreSkelWhitespace, boolean ignoreFragmentWhitespace,
										boolean ignoreSegmentation) {
		int i = 0;
		Event event1, event2;
		while (i < actual.size()) {
			event1 = actual.get(i);
			if (i >= expected.size()) {
				LOGGER.error("Less events in second list: {} vs {}", i, expected.size());
				return false;
			}
			event2 = expected.get(i);
			if (!compareEvent(event1, event2, includeSkeleton, ignoreSkelWhitespace, ignoreFragmentWhitespace, ignoreSegmentation)) {
				Event subDocEvent = subDocEvents.get(i);
				if (subDocEvent != null) {
					StartSubDocument ssd = (StartSubDocument) subDocEvent.getResource();
					if (ssd != null)
						LOGGER.error("Sub-document: {}", ssd.getName());
				}
				return false;
			}
			i++;
		}

		if (actual.size() != expected.size()) {
			LOGGER.error("Less events in first list: {} vs {}", actual.size(), expected.size());
			return false;
		}

		return true;
	}

	/**
	 * Creates a string output from a list of events.
	 *
	 * @param list           The list of events.
	 * @param encoderManager the encoder manager.
	 * @param trgLang        Code of the target (output) language.
	 * @return The generated output string
	 */
	public static String generateOutput(List<Event> list, EncoderManager encoderManager, LocaleId trgLang) {
		GenericSkeletonWriter writer = new GenericSkeletonWriter();
		return generateOutput(list, trgLang, writer, encoderManager, false);
	}

	/**
	 * Creates a string output from a list of events and upper-case the content of
	 * the target TUs.
	 *
	 * @param list           The list of events.
	 * @param encoderManager the encoder manager.
	 * @param trgLang        Code of the target (output) language.
	 * @return The generated output string
	 */
	public static String generateChangedOutput(List<Event> list, EncoderManager encoderManager, LocaleId trgLang) {
		GenericSkeletonWriter writer = new GenericSkeletonWriter();
		return generateOutput(list, trgLang, writer, encoderManager, true);
	}

	/**
	 * Creates a string output from a list of events, using a given ISkeletonWriter.
	 *
	 * @param list           the list of events.
	 * @param trgLang        code of the target (output) language.
	 * @param skelWriter     the ISkeletonWriter to use.
	 * @param encoderManager the encoder manager.
	 * @return The generated output string.
	 */
	public static String generateOutput(List<Event> list, LocaleId trgLang, ISkeletonWriter skelWriter,
			EncoderManager encoderManager) {
		return generateOutput(list, trgLang, skelWriter, encoderManager, false);
	}

	/**
	 * Creates a string output from a list of events, using a given ISkeletonWriter.
	 *
	 * @param list           the list of events.
	 * @param trgLang        code of the target (output) language.
	 * @param skelWriter     the ISkeletonWriter to use.
	 * @param encoderManager the encoder manager.
	 * @param changeTarget   true to change the content of the target TU.
	 * @return The generated output string.
	 */
	public static String generateOutput(List<Event> list, LocaleId trgLang, ISkeletonWriter skelWriter,
			EncoderManager encoderManager, boolean changeTarget) {
		StringBuilder tmp = new StringBuilder();
		for (Event event : list) {
			switch (event.getEventType()) {
			case START_DOCUMENT:
				tmp.append(skelWriter.processStartDocument(trgLang, "UTF-8", null, encoderManager,
						(StartDocument) event.getResource()));
				break;
			case END_DOCUMENT:
				tmp.append(skelWriter.processEndDocument((Ending) event.getResource()));
				break;
			case START_SUBDOCUMENT:
				tmp.append(skelWriter.processStartSubDocument((StartSubDocument) event.getResource()));
				break;
			case END_SUBDOCUMENT:
				tmp.append(skelWriter.processEndSubDocument((Ending) event.getResource()));
				break;
			case TEXT_UNIT:
				ITextUnit tu = event.getTextUnit();
				if (changeTarget) {
					TextContainer tc = tu.createTarget(trgLang, false, INameable.COPY_ALL);
					TextFragment tf = tc.getFirstContent();
					tf.setCodedText(tf.getCodedText().toUpperCase());
				}
				tmp.append(skelWriter.processTextUnit(tu));
				break;
			case DOCUMENT_PART:
				DocumentPart dp = (DocumentPart) event.getResource();
				tmp.append(skelWriter.processDocumentPart(dp));
				break;
			case START_GROUP:
				StartGroup startGroup = (StartGroup) event.getResource();
				tmp.append(skelWriter.processStartGroup(startGroup));
				break;
			case END_GROUP:
				tmp.append(skelWriter.processEndGroup((Ending) event.getResource()));
				break;
			case START_SUBFILTER:
				StartSubfilter startSubfilter = (StartSubfilter) event.getResource();
				tmp.append(skelWriter.processStartSubfilter(startSubfilter));
				break;
			case END_SUBFILTER:
				tmp.append(skelWriter.processEndSubfilter((EndSubfilter) event.getResource()));
				break;
			default:
				break;
			}
		}
		skelWriter.close();
		return tmp.toString();
	}

	public static String generateOutput(IFilter filter, List<Event> events, LocaleId locale, Charset encoding) {
		IFilterWriter fw = filter.createFilterWriter();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		fw.setOutput(baos);
		fw.setOptions(locale, encoding.toString());
		for (Event e : events) {
			fw.handleEvent(e);
		}
		return new String(baos.toByteArray(), encoding);
	}

	public static ITextUnit getTextUnit(IFilter filter, InputDocument doc, String defaultEncoding, LocaleId srcLang,
			LocaleId trgLang, int tuNumber) {
		return getTextUnit(filter, doc, defaultEncoding, srcLang, trgLang, tuNumber, false);
	}

	public static ITextUnit getTextUnitFromInputStream(IFilter filter, InputDocument doc, String defaultEncoding,
			LocaleId srcLang, LocaleId trgLang, int tuNumber) {
		return getTextUnit(filter, doc, defaultEncoding, srcLang, trgLang, tuNumber, true);
	}

	private static ITextUnit getTextUnit(IFilter filter, InputDocument doc, String defaultEncoding, LocaleId srcLang,
			LocaleId trgLang, int tuNumber, boolean inputStream) {
		try {
			// Load parameters if needed
			if (doc.paramFile == null || doc.paramFile.isEmpty()) {
				IParameters params = filter.getParameters();
				if (params != null) {
					// FIXME: shouldn't we take parameters as they are given?
					// params.reset();
				}
			} else {
				String root = Util.getDirectoryName(doc.path);
				IParameters params = filter.getParameters();
				if (params != null) {
					params.load(Util.toURL(root + "/" + doc.paramFile), false);
				}
			}
			// Open the input
			int num = 0;
			if (inputStream) {
				FileInputStream fileInputStream = new FileInputStream(doc.path);
				filter.open(new RawDocument(fileInputStream, defaultEncoding, srcLang, trgLang));
			} else {
				filter.open(new RawDocument((new File(doc.path)).toURI(), defaultEncoding, srcLang, trgLang));
			}
			// Process the document
			Event event;
			while (filter.hasNext()) {
				event = filter.next();
				if ((event.getEventType() == EventType.TEXT_UNIT) && (++num == tuNumber)) {
					return event.getTextUnit();
				}
			}
		} catch (FileNotFoundException ex) {
			LOGGER.error("File not found: {}", doc.path);
		} finally {
			if (filter != null) {
				filter.close();
			}
		}
		return null;
	}

	public static boolean testStartDocument(IFilter filter, InputDocument doc, String defaultEncoding, LocaleId srcLang,
			LocaleId trgLang) {
		try {
			// Load parameters if needed
			if (doc.paramFile == null || doc.paramFile.isEmpty()) {
				IParameters params = filter.getParameters();
				if (params != null)
					params.reset();
			} else {
				String root = Util.getDirectoryName(doc.path);
				IParameters params = filter.getParameters();
				if (params != null)
					params.load(Util.toURL(root + File.separator + doc.paramFile), false);
			}

			// Open the input
			filter.open(new RawDocument((new File(doc.path)).toURI(), defaultEncoding, srcLang, trgLang));
			// Process the document
			Event event;
			if (filter.hasNext()) {
				event = filter.next();
				assertSame("First event is not a StartDocument event.", EventType.START_DOCUMENT, event.getEventType());
				StartDocument sd = (StartDocument) event.getResource();
				assertNotNull("No StartDocument", sd);
				// Name may be null if not URL input assertNotNull("Name is null",
				// sd.getName());
				assertNotNull("Encoding is null", sd.getEncoding());
				assertNotNull("ID is null", sd.getId());
				assertNotNull("Language is null", sd.getLocale());
				assertNotNull("Linebreak is null", sd.getLineBreak());
				assertNotNull("FilterWriter is null", sd.getFilterWriter());
				assertNotNull("Mime type is null", sd.getMimeType());
				return true;
			}
		} finally {
			if (filter != null)
				filter.close();
		}
		return false;
	}

	/**
	 * Gets the Nth text unit found in the given list of events.
	 *
	 * @param list     The list of events
	 * @param tuNumber The number of the unit to return: 1 for the first one, 2 for
	 *                 the second, etc.
	 * @return The text unit found, or null.
	 */
	public static ITextUnit getTextUnit(List<Event> list, int tuNumber) {
		int n = 0;
		for (Event event : list) {
			if ((event.getEventType() == EventType.TEXT_UNIT) && (++n == tuNumber)) {
				return event.getTextUnit();
			}
		}
		return null;
	}

	/**
	 * Filter out and return only the ITextUnits from the provided events.
	 *
	 * @param events list of events to filter
	 * @return text units that were present in the filtered events
	 */
	public static List<ITextUnit> filterTextUnits(List<Event> events) {
		List<ITextUnit> tus = new ArrayList<>();
		for (Event e : events) {
			if (e.isTextUnit()) {
				tus.add(e.getTextUnit());
			}
		}
		return tus;
	}

	/**
	 * Return the number of events with the specified type.
	 *
	 * @param events events to be counted
	 * @param type   event type to be counted
	 * @return count of specific events
	 */
	public static int countEventsByType(List<Event> events, EventType type) {
		int count = 0;
		for (Event e : events) {
			if (type == e.getEventType()) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Gets the Nth group found in the given list of events.
	 *
	 * @param list        The list of events
	 * @param groupNumber The number of the group to return: 1 for the first one, 2
	 *                    for the second, etc.
	 * @return The group found, or null.
	 */
	public static StartGroup getGroup(List<Event> list, int groupNumber) {
		int n = 0;
		for (Event event : list) {
			if ((event.getEventType() == EventType.START_GROUP) && (++n == groupNumber)) {
				return (StartGroup) event.getResource();
			}
		}
		return null;
	}

	/**
	 * Gets the start document in the given list of events.
	 *
	 * @param list The list of events
	 * @return The start document found, or null.
	 */
	public static StartDocument getStartDocument(List<Event> list) {
		for (Event event : list) {
			if (event.getEventType() == EventType.START_DOCUMENT) {
				return (StartDocument) event.getResource();
			}
		}
		return null;
	}

	/**
	 * Gets the Nth start sub-document event in a given list of events.
	 *
	 * @param list         the list of events.
	 * @param subDocNumber the number of the sub-document to return 1 for first, 2
	 *                     for second, etc.
	 * @return the sub-document found or null.
	 */
	public static StartSubDocument getStartSubDocument(List<Event> list, int subDocNumber) {
		int n = 0;
		for (Event event : list) {
			if ((event.getEventType() == EventType.START_SUBDOCUMENT) && (++n == subDocNumber)) {
				return (StartSubDocument) event.getResource();
			}
		}
		return null;
	}

	/**
	 * Gets the Nth document part found in the given list of events.
	 *
	 * @param list     The list of events
	 * @param dpNumber The number of the document part to return: 1 for the first
	 *                 one, 2 for the second, etc.
	 * @return The document part found, or null.
	 */
	public static DocumentPart getDocumentPart(List<Event> list, int dpNumber) {
		int n = 0;
		for (Event event : list) {
			if ((event.getEventType() == EventType.DOCUMENT_PART) && (++n == dpNumber)) {
				return (DocumentPart) event.getResource();
			}
		}
		return null;
	}

	public static boolean compareTextUnit(ITextUnit actual, ITextUnit expected, boolean ignoreFragmentWhitespace, boolean ignoreSegmentation) {
		if (!compareINameable(actual, expected, ignoreFragmentWhitespace)) {
			LOGGER.error("Difference in INameable");
			return false;
		}
		if (actual.isReferent() != expected.isReferent()) {
			LOGGER.error("TEXT_UNIT isReferent difference: {} vs {}", actual.isReferent(), expected.isReferent());
			return false;
		}

		// FIXME: skip xliff2 until we work out whitespace handling. Unfortunately this blocks xliff1.2 as well.
		if (!MimeTypeMapper.XLIFF2_MIME_TYPE.equals(expected.getMimeType())) {
			if (actual.preserveWhitespaces() != expected.preserveWhitespaces()) {
				LOGGER.error("preserveWhitespaces difference: {} vs {}", actual.preserveWhitespaces(), expected.preserveWhitespaces());
				return false;
			}
		}

		if (!compareTextContainer(actual.getSource(), expected.getSource(), ignoreFragmentWhitespace, ignoreSegmentation)) {
			LOGGER.error("TextContainer difference");
			return false;
		}
		// if the original has a target (and the text is non-empty) compare with new one
		// only test one target locale
		if (!Util.isEmpty(expected.getTargetLocales())) {
			Iterator<LocaleId> locIter = expected.getTargetLocales().iterator();
			// find the right target locale that matched actual
			LocaleId targetLocale = null;
			while (locIter.hasNext()) {
				LocaleId sl = locIter.next();
				if (actual.hasTarget(sl) && actual.getTarget(sl).hasText()) {
					targetLocale = sl;
					break;
				}
			}

			if (targetLocale != null) {
				if (!compareTextContainer(actual.getTarget(targetLocale), expected.getTarget(targetLocale),
						ignoreFragmentWhitespace, true)) {
					LOGGER.error("TextContainer difference");
					return false;
				}
			}
		}

		return true;
	}

	public static boolean compareIResource(IResource actual, IResource expected, boolean includeSkeleton,
			boolean ignoreSkelWhitespace) {
		if (actual == null && expected == null) {
			return true;
		}
		if (actual == null || expected == null) {
			LOGGER.error("IResource, only one IResource defined: {} vs {}", actual, expected);
			return false;
		}

		// ID
		String tmp1 = actual.getId();
		String tmp2 = expected.getId();
		if (tmp1 == null && tmp2 == null) {
			return true;
		}
		if (tmp1 == null || tmp2 == null) {
			LOGGER.error("IResource only one IResource has an ID: {} vs {}", tmp1, tmp2);
			return false;
		}
		if (!tmp1.equals(tmp2)) {
			LOGGER.error("IResource ID difference: '{}' vs '{}'", tmp1, tmp2);
			return false;
		}

		// Skeleton
		if (!includeSkeleton) {
			return true;
		}

		String id1 = actual.getId();

		ISkeleton skl1 = actual.getSkeleton();
		ISkeleton skl2 = expected.getSkeleton();
		if (skl1 == null && skl2 == null) {
			return true;
		}
		if (skl1 == null || skl2 == null) {
			LOGGER.error("only one IResource has a skeleton ()");
			return false;
		}
		tmp1 = skl1.toString();
		tmp2 = skl2.toString();
		if (tmp1 == null && tmp2 == null) {
			return true;
		}
		if (tmp1 == null || tmp2 == null) {
			LOGGER.error("only one IResource has a skeleton");
			return false;
		}

		if (skl1 instanceof GenericSkeleton && skl2 instanceof GenericSkeleton) {
			if (ignoreSkelWhitespace) {
				String ntmp1 = normalize(tmp1);
				String ntmp2 = normalize(tmp2);
				if (!ntmp1.equals(ntmp2)) {
					LOGGER.error("IResource, Skeleton differences in {}:\n 1='{}'\n 2='{}'", id1, ntmp1, ntmp2);
					return false;
				}
			} else {
				if (!tmp1.equals(tmp2)) {
					LOGGER.error("IResource, Skeleton differences in {}:\n 1='{}'\n 2='{}'", id1, tmp1, tmp2);
					return false;
				}
			}
		}
		return true;
	}

	public static boolean compareINameable(INameable actual, INameable expected, boolean ignoreFragmentWhitespace) {
		if (actual == null && expected == null) {
			return true;
		}
		if (actual == null || expected == null) {
			LOGGER.error("only one INameable defined: {} vs {}", actual, expected);
			return false;
		}

		// Resource-level properties
		Set<String> names1 = actual.getPropertyNames();
		Set<String> names2 = expected.getPropertyNames();

		// segmentation adds this - don't compare
		names1.remove("wassegmented");
		names2.remove("wassegmented");

		if (names1.size() != names2.size()) {
			LOGGER.error("INameable, resource-level property names difference:\n 1='{}'\n 2='{}'", names1, names2);
			return false;
		}
		for (String name : actual.getPropertyNames()) {
			Property p1 = actual.getProperty(name);
			Property p2 = expected.getProperty(name);
			if (ignoreFragmentWhitespace) {
				p1.setValue(normalize(p1.getValue()));
				p2.setValue(normalize(p2.getValue()));
			}

			if (!compareProperty(p1, p2)) {
				return false;
			}
		}

		if (actual instanceof IMultilingual && expected instanceof IMultilingual) {
			IMultilingual m1 = (IMultilingual) actual;
			IMultilingual m2 = (IMultilingual) expected;

			// Source properties
			names1 = m1.getSourcePropertyNames();
			names2 = m2.getSourcePropertyNames();
			names1.remove("wassegmented");
			names2.remove("wassegmented");

			if (names1.size() != names2.size()) {
				LOGGER.error("INameable, source property names difference:\n1='{}'\n2='{}'", names1, names2);
				return false;
			}
			for (String name : m1.getSourcePropertyNames()) {
				Property p1 = m1.getSourceProperty(name);
				Property p2 = m2.getSourceProperty(name);
				if (!compareProperty(p1, p2)) {
					return false;
				}
			}
		}

		// Target properties
		// TODO: Target properties

		// Name
		String tmp1 = actual.getName();
		String tmp2 = expected.getName();
		if (tmp1 == null) {
			if (tmp2 != null) {
				LOGGER.error("INameable, name null difference: null vs '{}'", tmp2);
				return false;
			}
		} else {
			if (tmp2 == null) {
				LOGGER.error("INameable, name null difference: '{}' vs null", tmp1);
				return false;
			}
			if (!tmp1.equals(tmp2)) {
				LOGGER.error("INameable, name difference: '{}' vs '{}'", tmp1, tmp2);
				return false;
			}
		}

		// Type
		tmp1 = actual.getType();
		tmp2 = expected.getType();
		if (tmp1 == null) {
			if (tmp2 != null) {
				LOGGER.error("INameable, type null difference: null vs '{}'", tmp2);
				return false;
			}
		} else {
			if (tmp2 == null) {
				LOGGER.error("INameable, type null difference: '{}' vs null", tmp1);
				return false;
			}
			if (!tmp1.equals(tmp2)) {
				LOGGER.error("INameable, type difference: '{}' vs '{}'", tmp1, tmp2);
				return false;
			}
		}

		// MIME type
		tmp1 = actual.getMimeType();
		tmp2 = expected.getMimeType();
		if (tmp1 == null) {
			if (tmp2 != null) {
				LOGGER.error("INameable, mime-type null difference: null vs '{}'", tmp2);
				return false;
			}
		} else {
			if (tmp2 == null) {
				LOGGER.error("INameable, mime-type null difference: '{}' vs null", tmp1);
				return false;
			}
			if (!tmp1.equals(tmp2)) {
				LOGGER.error("INameable, mime-type difference: '{}' vs '{}'", tmp1, tmp2);
				return false;
			}
		}

		// Is translatable
		if (actual.isTranslatable() != expected.isTranslatable()) {
			LOGGER.error("INameable, isTranslatable difference: {} vs {}", actual.isTranslatable(),
					expected.isTranslatable());
			return false;
		}

		return true;
	}

	public static boolean compareProperty(Property actual, Property expected) {
		if (actual == null) {
			if (expected != null) {
				LOGGER.error("Property null difference: {} vs {}", actual, expected);
				return false;
			}
			return true;
		}
		if (expected == null) {
			LOGGER.error("Property null difference: {} vs {}", actual, expected);
			return false;
		}

		if (!actual.getName().equals(expected.getName())) {
			LOGGER.error("Property name difference: {} vs {}", actual.getName(), expected.getName());
			return false;
		}
		if (actual.isReadOnly() != expected.isReadOnly()) {
			LOGGER.error("Property isReadOnly difference: {} vs {}", actual.isReadOnly(), expected.isReadOnly());
			return false;
		}
		if (actual.getValue() == null) {
			if (expected.getValue() != null) {
				LOGGER.error("Property value null difference: {} vs {}", actual.getValue(), expected.getValue());
				return false;
			}
			return true;
		}

		// we don't care about encoding and locale differences these change often in
		// target files
		// "Parent Scalar Indent" can change for Yaml files
		// "Scalar Type" can change
		if (!"encoding".equals(actual.getName()) && !"language".equals(actual.getName())
				&& !"Parent Scalar Indent".equals(actual.getName()) && !"Scalar Type".equals(actual.getName())) {
			// In double-extraction 'start' can be different
			if (!actual.getValue().equals(expected.getValue())
					&& (!actual.getName().equals("start") && !actual.getName().equals("Quote Char"))) {
				LOGGER.error("Property value null difference {}: {} vs {}", actual.getName(), actual.getValue(),
						expected.getValue());
				return false;
			}
		}
		return true;
	}

	public static boolean compareTextContainer(TextContainer actual, TextContainer expected) {
		return compareTextContainer(actual, expected, false, false);
	}

	public static boolean compareTextContainer(TextContainer actual, TextContainer expected, boolean ignoreFragmentWhitespace, boolean ignoreSegmentation) {
		if (actual == null) {
			if (expected != null) {
				LOGGER.error("Text container null difference: {} vs {}", actual, expected);
				return false;
			}
			return true;
		}
		if (expected == null) {
			LOGGER.error("Text container null difference: {} vs {}", actual, expected);
			return false;
		}

		try {
			if (!compareTextFragment(actual.getUnSegmentedContentCopy(), expected.getUnSegmentedContentCopy(),
					ignoreFragmentWhitespace)) {
				LOGGER.error("Fragment difference");
				return false;
			}
		} catch (ParserConfigurationException e) {
			LOGGER.error("Parse Error: {} vs {}", actual, expected);
			return false;
		}

		if (ignoreSegmentation) {
			return true;
		}

		if (actual.hasBeenSegmented()) {
			if (!expected.hasBeenSegmented()) {
				LOGGER.error("TextContainer isSegmented difference: {} vs {}", actual.hasBeenSegmented(),
						expected.hasBeenSegmented());
				return false;
			}
			ISegments t1Segments = actual.getSegments();
			ISegments t2Segments = expected.getSegments();
			if (t1Segments.count() != t2Segments.count()) {
				LOGGER.error("TextContainer, number of segments difference: {} vs {}", t1Segments.count(),
						t2Segments.count());
				return false;
			}

			for (Segment seg1 : t1Segments) {
				Segment seg2 = t2Segments.get(seg1.id);
				if (seg2 == null) {
					LOGGER.error("TextContainer, segment in expected not found.");
					return false;
				}

				if (seg1.whitespaceStrategy != seg2.whitespaceStrategy) {
					LOGGER.error("whitespaceStrategy difference: {} vs {}", seg1.whitespaceStrategy, seg2.whitespaceStrategy);
					return false;
				}

				try {
					if (!compareTextFragment(seg1.text, seg2.text, ignoreFragmentWhitespace)) {
						LOGGER.error("Text fragment difference");
						return false;
					}
				} catch (ParserConfigurationException e) {
					LOGGER.error("Parse Error: {} vs {}", actual, expected);
					return false;
				}
			}
		} else {
			if (expected.hasBeenSegmented()) {
				LOGGER.error("TextContainer isSegmented difference: {} vs {}", actual.hasBeenSegmented(),
						expected.hasBeenSegmented());
				return false;
			}
		}

		return true;
	}

	public static boolean compareTextFragment(TextFragment actual, TextFragment expected,
			boolean ignoreFragmentWhitespace) throws ParserConfigurationException {
		if (actual == null) {
			if (expected != null) {
				LOGGER.error("TextFragment null difference: {} vs {}", actual, expected);
				return false;
			}
			return true;
		}
		if (expected == null) {
			LOGGER.error("TextFragment null difference: {} vs {}", actual, expected);
			return false;
		}

		List<Code> codes1 = actual.getCodes();
		List<Code> codes2 = expected.getCodes();
		if (codes1.size() != codes2.size()) {
			LOGGER.error("Number of codes difference: {} vs {}\n 1={}\n 2={}", codes1.size(), codes2.size(),
					codes1.toString(), codes2.toString());
			return false;
		}
		for (int i = 0; i < codes1.size(); i++) {
			Code code1 = codes1.get(i);
			Code code2 = codes2.get(i);

			// FIXME: apparently segmentation can change code ids
			// for example TXML segmented roundtrip fails here
			if (code1.getId() != code2.getId()) {
				LOGGER.error("Code ID difference: {} vs {}", code1.getId(), code2.getId());
				return false;
			}
			// Data
			String tmp1 = code1.getData();
			String tmp2 = code2.getData();
			if (tmp1 == null) {
				if (tmp2 != null) {
					LOGGER.error("Code data null difference: {} vs {}", tmp1, tmp2);
					return false;
				}
			} else {
				if (tmp2 == null) {
					LOGGER.error("Code data null difference: {} vs {}", tmp1, tmp2);
					return false;
				}

				if (ignoreFragmentWhitespace) {
					if (!normalize(tmp1).equals(normalize(tmp2))) {
						LOGGER.error("Code data difference: {} vs {}", tmp1, tmp2);
						return false;
					}
				} else {
					if (!tmp1.equals(tmp2)) {
						LOGGER.error("Code data difference: {} vs {}", tmp1, tmp2);
						return false;
					}
				}
			}
			// Outer data can be xml - no easy way to compare with attribute reordering,
			// non-wellformed etc.. This must do for now
			tmp1 = code1.getOuterData();
			tmp2 = code2.getOuterData();
			if (Util.isEmpty(tmp1)) {
				if (!Util.isEmpty(tmp2)) {
					LOGGER.error("Code outer data null difference: {} vs {}", tmp1, tmp2);
					return false;
				}
			} else {
				if (Util.isEmpty(tmp2)) {
					LOGGER.error("Code outer null difference: {} vs {}", tmp1, tmp2);
					return false;
				}
			}

			// Type
			tmp1 = code1.getType();
			tmp2 = code2.getType();
			if (tmp1 == null) {
				if (tmp2 != null) {
					LOGGER.error("Code type null difference: {} vs {}", tmp1, tmp2);
					return false;
				}
			} else {
				if (tmp2 == null) {
					LOGGER.error("Code type null difference: {} vs {}", tmp1, tmp2);
					return false;
				}
				if (!tmp1.equals(tmp2)) {
					LOGGER.error("Code type difference: {} vs {}", tmp1, tmp2);
					return false;
				}
			}
			// Tag type
			if (code1.getTagType() != code2.getTagType()) {
				LOGGER.error("Code tag-type difference: {} vs {}", code1.getTagType(), code2.getTagType());
				return false;
			}
			if (code1.hasReference() != code2.hasReference()) {
				LOGGER.error("Code hasReference difference: {} vs {}", code1.hasReference(), code2.hasReference());
				return false;
			}
			if (code1.isCloneable() != code2.isCloneable()) {
				LOGGER.error("Code isCloneable difference: {} vs {}", code1.isCloneable(), code2.isCloneable());
				return false;
			}
			if (code1.isDeleteable() != code2.isDeleteable()) {
				LOGGER.error("Code isDeleteable difference: {} vs {}", code1.isDeleteable(), code2.isDeleteable());
				return false;
			}
			if (code1.hasAnnotation() != code2.hasAnnotation()) {
				LOGGER.error("Code annotation difference: {} vs {}", code1.hasAnnotation(), code2.hasAnnotation());
				return false;
			}
			// TODO: compare annotations
		}

		if (ignoreFragmentWhitespace) {
			// Coded text
			String nActual = normalize(actual.getCodedText());
			String nExpected = normalize(expected.getCodedText());
			if (!nActual.equals(nExpected)) {
				LOGGER.error("Code, normalized coded text difference:\n 1='{}'\n 2='{}'", nActual, nExpected);
				return false;
			}
		} else {
			// Coded text
			if (!actual.getCodedText().equals(expected.getCodedText())) {
				LOGGER.error("Code, coded text difference:\n 1='{}'\n 2='{}'", actual.getCodedText(),
						expected.getCodedText());
				return false;
			}

		}
		return true;
	}

	/*
	 * Remove all whitespace, including newlines. Also remove xml:space=\"preserve\"
	 * and <w:r><w:t> as some filter writers (openxml) introduce these in the inline
	 * codes.
	 */
	public static String normalize(String string) {
		if (Util.isEmpty(string)) {
			return string;
		}
		String res = StringUtil.normalizeLineBreaks(string);
		res = PATTERN_WHITESPACE.matcher(res).replaceAll("");
		return res;
	}

	static public ArrayList<Event> getTextUnitEvents(IFilter filter, RawDocument rd) {
		ArrayList<Event> list = new ArrayList<>();
		try {
			filter.open(rd);
			while (filter.hasNext()) {
				Event e = filter.next();
				if (e.isTextUnit()) {
					list.add(e);
				}
			}
		} finally {
			filter.close();
		}
		return list;
	}

	/**
	 * Indicates to this driver to display the skeleton data.
	 *
	 * @param value True to display the skeleton, false to not display the skeleton.
	 */
	public void setShowSkeleton(boolean value) {
		showSkeleton = value;
	}

	/**
	 * Indicates what to display.
	 *
	 * @param value 0=display nothing, 1=display TU only, >1=display all.
	 */
	public void setDisplayLevel(int value) {
		displayLevel = value;

	}

	/**
	 * Process the input document. You must have called the setOptions() and open()
	 * methods of the filter before calling this method.
	 *
	 * @param filter Filter to process.
	 * @return False if an error occurred, true if all was OK.
	 */
	@SuppressWarnings("unused")
	public boolean process(IFilter filter) {
		ok = true;
		int startDoc = 0;
		int endDoc = 0;
		int startGroup = 0;
		int endGroup = 0;
		int startSubDoc = 0;
		int endSubDoc = 0;
		Event event;
		while (filter.hasNext()) {
			event = filter.next();
			switch (event.getEventType()) {
			case START_DOCUMENT:
				startDoc++;
				checkStartDocument((StartDocument) event.getResource());
				if (displayLevel < 2)
					break;
				LOGGER.trace("---Start Document");
				printSkeleton(event.getResource());
				break;
			case END_DOCUMENT:
				endDoc++;
				if (displayLevel < 2)
					break;
				LOGGER.trace("---End Document");
				printSkeleton(event.getResource());
				break;
			case START_SUBDOCUMENT:
				startSubDoc++;
				if (displayLevel < 2)
					break;
				LOGGER.trace("---Start Sub Document");
				printSkeleton(event.getResource());
				break;
			case END_SUBDOCUMENT:
				endSubDoc++;
				if (displayLevel < 2)
					break;
				LOGGER.trace("---End Sub Document");
				printSkeleton(event.getResource());
				break;
			case START_GROUP:
				startGroup++;
				if (displayLevel < 2)
					break;
				LOGGER.trace("---Start Group");
				printSkeleton(event.getResource());
				break;
			case END_GROUP:
				endGroup++;
				if (displayLevel < 2)
					break;
				LOGGER.trace("---End Group");
				printSkeleton(event.getResource());
				break;
			case TEXT_UNIT:
				ITextUnit tu = event.getTextUnit();
				if (displayLevel < 1)
					break;
				printTU(tu);
				if (displayLevel < 2)
					break;
				printResource(tu);
				printSkeleton(tu);
				break;
			case DOCUMENT_PART:
				if (displayLevel < 2)
					break;
				LOGGER.trace("---Document Part");
				printResource((INameable) event.getResource());
				printSkeleton(event.getResource());
				break;
			case START_SUBFILTER:
				if (displayLevel < 2)
					break;
				LOGGER.trace("---Start Subfilter");
				printSkeleton(event.getResource());
				break;
			case END_SUBFILTER:
				if (displayLevel < 2)
					break;
				LOGGER.trace("---End Subfilter");
				printSkeleton(event.getResource());
				break;
			default:
				break;
			}
		}

		if (startDoc != 1) {
			LOGGER.error("ERROR: START_DOCUMENT = {}", startDoc);
			ok = false;
		}
		if (endDoc != 1) {
			LOGGER.error("ERROR: END_DOCUMENT = {}", endDoc);
			ok = false;
		}
		if (startSubDoc != endSubDoc) {
			LOGGER.error("ERROR: START_SUBDOCUMENT={}, END_SUBDOCUMENT={}", startSubDoc, endSubDoc);
			ok = false;
		}
		if (startGroup != endGroup) {
			LOGGER.error("ERROR: START_GROUP={}, END_GROUP={}", startGroup, endGroup);
			ok = false;
		}
		return ok;
	}

	private void printTU(ITextUnit tu) {
		LOGGER.trace("---Text Unit");
		LOGGER.trace("S=[{}]", tu);
		for (LocaleId lang : tu.getTargetLocales()) {
			LOGGER.trace("T({})=[{}]", lang, tu.getTarget(lang));
		}
	}

	private void printResource(INameable res) {
		if (res == null) {
			LOGGER.trace("NULL resource.");
			ok = false;
		} else {
			LOGGER.trace("  id='{}'", res.getId());
			LOGGER.trace(" name='{}'", res.getName());
			LOGGER.trace(" type='{}'", res.getType());
			LOGGER.trace(" mimeType='{}'", res.getMimeType());
		}
	}

	private void printSkeleton(IResource res) {
		if (!showSkeleton)
			return;
		ISkeleton skel = res.getSkeleton();
		if (skel != null) {
			LOGGER.trace("---");
			LOGGER.trace(skel.toString());
			LOGGER.trace("---");
		}
	}

	private void checkStartDocument(StartDocument startDoc) {
		if (displayLevel < 1)
			return;
		String tmp = startDoc.getEncoding();
		if ((tmp == null) || (tmp.length() == 0)) {
			LOGGER.error("WARNING: No encoding specified in StartDocument.");
		} else if (displayLevel > 1)
			LOGGER.trace("StartDocument encoding = {}", tmp);

		LocaleId locId = startDoc.getLocale();
		if (Util.isNullOrEmpty(locId)) {
			LOGGER.error("WARNING: No language specified in StartDocument.");
		} else if (displayLevel > 1)
			LOGGER.trace("StartDocument language = {}", locId);

		tmp = startDoc.getName();
		if ((tmp == null) || (tmp.length() == 0)) {
			LOGGER.error("WARNING: No name specified in StartDocument.");
		} else if (displayLevel > 1)
			LOGGER.trace("StartDocument name = {}", tmp);

		if (displayLevel < 2)
			return;
		LOGGER.error("StartDocument MIME type = {}", startDoc.getMimeType());
		LOGGER.error("StartDocument Type = {}", startDoc.getType());
	}
}

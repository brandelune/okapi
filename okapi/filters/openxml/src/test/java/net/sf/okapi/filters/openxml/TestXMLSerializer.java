package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.FileLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xml.sax.InputSource;
import org.xmlunit.matchers.CompareMatcher;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class TestXMLSerializer {
	private XMLInputFactory inputFactory = XMLInputFactory.newInstance();
	private XMLEventFactory eventFactory = XMLEventFactory.newInstance();
	private XMLEventSerializer s;

	@Before
	public void setup() {
		s = new XMLEventSerializer();
	}

	@Test
	public void test() throws Exception {
		for (XMLEvent e : getEvents("/parts/simplifier/document-spelling.xml")) {
			s.add(e);
		}
		assertThat(new InputSource(getGoldReader("document-spelling.xml")),
				CompareMatcher.isSimilarTo(new InputSource(new StringReader(s.toString()))));

	}

	@Test
	public void testChars() throws Exception {
		s.add(eventFactory.createCharacters("ABC><&'\"!"));
		assertEquals("ABC&gt;&lt;&amp;'\"!", s.toString());
	}

	@Test
	public void testAttrQuoting() throws Exception {
		for (XMLEvent e : getEvents("/serializer/attrquoting.xml")) {
			s.add(e);
		}
		assertThat(new InputSource(getGoldReader("attrquoting.xml")),
				CompareMatcher.isSimilarTo(new InputSource(new StringReader(s.toString()))));
	}

	private Reader getGoldReader(String name) {
		final FileLocation.In location = FileLocation.fromClass(getClass()).in("/gold/serializer/" + name);
		return new InputStreamReader(location.asInputStream(), StandardCharsets.UTF_8);
	}

	private List<XMLEvent> getEvents(String name) throws XMLStreamException {
		final FileLocation.In location = FileLocation.fromClass(getClass()).in(name);
		XMLEventReader xmlReader = inputFactory.createXMLEventReader(location.asInputStream(), "UTF-8");
		List<XMLEvent> events = new ArrayList<>();
		while (xmlReader.hasNext()) {
			events.add(xmlReader.nextEvent());
		}
		return events;
	}

}

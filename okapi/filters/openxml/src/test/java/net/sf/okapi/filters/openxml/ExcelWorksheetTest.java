package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.FileLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.DifferenceEvaluators;
import org.xmlunit.matchers.CompareMatcher;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class ExcelWorksheetTest {
	private static final String WORKSHEET_NAME = "test";
	private final XMLFactories xmlfactories = new XMLFactoriesForTest();
	private FileLocation root;

	@Before
	public void setUp() {
		root = FileLocation.fromClass(getClass());
	}

	@Test
	public void test() throws Exception {
		SharedStrings ssm = new SharedStrings();
		ExcelWorksheet parser = new ExcelWorksheet(xmlfactories.getEventFactory(), ssm, new ExcelStyles(), false,
				new HashSet<>(), new HashSet<>(), true, WORKSHEET_NAME);
		String output = parseWorksheet(parser, "/xlsx_parts/sheet1.xml");
		assertThat(Input.fromStream(root.in("/xlsx_parts/gold/Rewritten_sheet1.xml").asInputStream()),
				CompareMatcher.isIdenticalTo(output)
						.withDifferenceEvaluator(DifferenceEvaluators.ignorePrologDifferences()));
	}

	@Test
	public void testExcludeColors() throws Exception {
		SharedStrings ssm = new SharedStrings();
		ExcelStyles styles = new ExcelStyles();
		styles.parse(xmlfactories.getInputFactory().createXMLEventReader(
				root.in("/xlsx_parts/rgb_styles.xml").asInputStream(), "UTF-8"));
		Set<String> excludedColors = new HashSet<>();
		excludedColors.add("FF800000");
		excludedColors.add("FFFF0000");
		ExcelWorksheet parser = new ExcelWorksheet(xmlfactories.getEventFactory(), ssm, styles, false,
				new HashSet<>(), excludedColors, true, WORKSHEET_NAME);
		parseWorksheet(parser, "/xlsx_parts/rgb_sheet1.xml");
		List<SharedStrings.Item> entries = ssm.items();
		assertTrue(entries.get(0).excluded());  // excluded due to FF800000
		assertTrue(entries.get(1).excluded());  // excluded due to FFFF0000
		assertFalse(entries.get(2).excluded());
		assertFalse(entries.get(3).excluded());
		assertFalse(entries.get(4).excluded());
		assertFalse(entries.get(5).excluded());
		assertFalse(entries.get(6).excluded());
		assertFalse(entries.get(7).excluded());
		assertFalse(entries.get(8).excluded());
		assertFalse(entries.get(9).excluded());
	}

	@Test
	public void testExcludeHiddenCells() throws Exception {
		SharedStrings ssm = new SharedStrings();
		ExcelWorksheet parser = new ExcelWorksheet(xmlfactories.getEventFactory(), ssm, new ExcelStyles(), false,
				new HashSet<>(), new HashSet<>(), true, WORKSHEET_NAME);
		parseWorksheet(parser, "/xlsx_parts/worksheet-hiddenCells.xml");
		List<SharedStrings.Item> entries = ssm.items();
		assertFalse(entries.get(0).excluded());
		assertTrue(entries.get(1).excluded());
		assertTrue(entries.get(2).excluded());
		assertTrue(entries.get(3).excluded());
	}

	@Test
	public void testExposeHiddenCells() throws Exception {
		SharedStrings ssm = new SharedStrings();
		ExcelWorksheet parser = new ExcelWorksheet(xmlfactories.getEventFactory(), ssm, new ExcelStyles(), false,
				new HashSet<>(), new HashSet<>(), false, WORKSHEET_NAME);
		parseWorksheet(parser, "/xlsx_parts/worksheet-hiddenCells.xml");
		List<SharedStrings.Item> entries = ssm.items();
		assertFalse(entries.get(0).excluded());
		assertFalse(entries.get(1).excluded());
		assertFalse(entries.get(2).excluded());
		assertFalse(entries.get(3).excluded());
	}

	private String parseWorksheet(ExcelWorksheet parser, String resourceName) throws Exception {
		XMLEventReader reader = xmlfactories.getInputFactory().createXMLEventReader(
				root.in(resourceName).asInputStream(), "UTF-8");
		StringWriter sw = new StringWriter();
		XMLEventWriter writer = xmlfactories.getOutputFactory().createXMLEventWriter(sw);
		parser.parse(reader, writer);
		reader.close();
		writer.close();
		return sw.toString();
	}

	@Test
	public void testIndexToColumnName() {
		assertEquals("A", ExcelWorksheet.indexToColumnName(1));
		assertEquals("Z", ExcelWorksheet.indexToColumnName(26));
		assertEquals("AA", ExcelWorksheet.indexToColumnName(27));
		assertEquals("AZ", ExcelWorksheet.indexToColumnName(52));
		assertEquals("BA", ExcelWorksheet.indexToColumnName(53));
	}
}

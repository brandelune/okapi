/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class SharedStrings {
	private static final int START_INDEX = 0;
	private int nextIndex = START_INDEX;
	private final ArrayList<Item> items;

	SharedStrings() {
		this.items = new ArrayList<>();
	}

	int nextIndex() {
		return this.nextIndex;
	}

	void add(final Item item) {
		this.items.ensureCapacity(item.newIndex() + 1);
		this.items.add(item.newIndex(), item);
		this.nextIndex++;
	}

	List<Item> items() {
		return items;
	}

	boolean worksheetStartsAt(final int index) {
		if (START_INDEX == index) {
			return true;
		}
		return !this.items.get(index - 1).worksheet().equals(this.items.get(index).worksheet());
	}

	String worksheetAt(int index) {
		return items.get(index).worksheet();
	}

	boolean rowStartsAt(final int index) {
		return !this.items.get(index - 1).rowIndex().equals(this.items.get(index).rowIndex());
	}

	String rowAt(final int index) {
		return this.items.get(index).rowIndex;
	}

	boolean visibleAt(int index) {
		return !items.get(index).excluded;
	}

	String cellReferenceAt(int index) {
		return items.get(index).cellReference();
	}

	static class Item {
		private int origIndex;
		private int newIndex;
		private String worksheet;
		private String rowIndex;
		private String columnIndex;
		private boolean excluded;

		Item(
			final int origIndex,
			final int newIndex,
			final String worksheet,
			final String rowIndex,
			final String columnIndex,
			final boolean excluded
		) {
			this.origIndex = origIndex;
			this.newIndex = newIndex;
			this.worksheet = worksheet;
			this.rowIndex = rowIndex;
			this.columnIndex = columnIndex;
			this.excluded = excluded;
		}

		int originalIndex() {
			return origIndex;
		}

		int newIndex() {
			return newIndex;
		}

		String worksheet() { return worksheet; }

		String rowIndex() {
			return this.rowIndex;
		}

		String columnIndex() {
			return this.columnIndex;
		}

		boolean excluded() {
			return excluded;
		}

		String cellReference() { return this.columnIndex.concat(this.rowIndex); }

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || !(o instanceof Item)) return false;
			Item e = (Item)o;
			return origIndex == e.origIndex && newIndex == e.newIndex;
		}
		@Override
		public int hashCode() {
			return Objects.hash(origIndex, newIndex);
		}

		@Override
		public String toString() {
			return "Item("
				+ origIndex + " --> " + newIndex + ", "
				+ worksheet + ", "
				+ columnIndex + rowIndex + ", "
				+ (excluded ? "excluded" : "visible")
				+ ")";
		}
	}
}
